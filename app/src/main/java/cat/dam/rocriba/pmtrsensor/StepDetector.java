package cat.dam.rocriba.pmtrsensor;

public class StepDetector {

    //Declarem variables per detectar els pasos
    private static final int ACCEL_RING_SIZE = 50;
    private static final int VEL_RING_SIZE = 10;

    //Canviem la prioiritat segons la sensebilitat que volguem
    private static final float STEP_THRESHOLD = 100f;

    private static final int STEP_DELAYS_NS = 250000000;

    private int accelRingCounter = 0;
    private float[] accelRingX = new float[ACCEL_RING_SIZE];
    private float[] accelRingY = new float[ACCEL_RING_SIZE];
    private float[] accelRingZ = new float[ACCEL_RING_SIZE];
    private int velRingCounter = 0;
    private float[] velRing = new float[VEL_RING_SIZE];
    private long lastStepTimeNs = 0;
    private float oldVelocityEstimate = 0;

    private PasListener listener;

    //Afegim el constructor
    public void registerListener(PasListener listener){
        this.listener = listener;
    }

    //en cas de rebre noves dades, actualitzem els valors i comprovem matemàticament si s'ha fet la passa o no.
    public void  updateAccel(long timeNS, float x, float y, float z){
        float[] currentAceel = new float[3];
        currentAceel[0] = x;
        currentAceel[1] = y;
        currentAceel[2] = z;

        //Al primer pas actualitzem el vector z
        accelRingCounter++;
        accelRingX[accelRingCounter % ACCEL_RING_SIZE] = currentAceel[0];
        accelRingY[accelRingCounter % ACCEL_RING_SIZE] = currentAceel[1];
        accelRingZ[accelRingCounter % ACCEL_RING_SIZE] = currentAceel[2];

        float[] worldZ = new float[3];
        worldZ[0] = SensorFilter.sum(accelRingX) / Math.min(accelRingCounter, ACCEL_RING_SIZE);
        worldZ[1] = SensorFilter.sum(accelRingY) / Math.min(accelRingCounter, ACCEL_RING_SIZE);
        worldZ[2] = SensorFilter.sum(accelRingZ) / Math.min(accelRingCounter, ACCEL_RING_SIZE);

        float normalitzation_factor = SensorFilter.norm(worldZ);

        worldZ[0] = worldZ[0] / normalitzation_factor;
        worldZ[1] = worldZ[1] / normalitzation_factor;
        worldZ[2] = worldZ[2] / normalitzation_factor;

        float currentZ = SensorFilter.dot(worldZ, currentAceel) - normalitzation_factor;
        velRingCounter++;
        velRing[velRingCounter % VEL_RING_SIZE] = currentZ;

        float velocityEstimate = SensorFilter.sum(velRing);

        if (velocityEstimate > STEP_THRESHOLD && oldVelocityEstimate <= STEP_THRESHOLD && (timeNS - lastStepTimeNs > STEP_DELAYS_NS)){
            listener.step(timeNS);
            lastStepTimeNs = timeNS;
        }
        oldVelocityEstimate = velocityEstimate;



    }




}
