package cat.dam.rocriba.pmtrsensor;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView textView, tv_passos;
    private Button btn_start, btn_stop;
    private StepDetector Detectorpasses;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = "Número de pasos: ";
    private int numSteps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //Agafem una instància de SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Detectorpasses = new StepDetector();
        Detectorpasses.registerListener(new PasListener() {
            @Override
            public void step(long timeNs) {

                numSteps++;
                tv_passos.setText(TEXT_NUM_STEPS + numSteps);
            }
        });

        //Iinicilaitzem els views i buttons
        tv_passos = (TextView) findViewById(R.id.tv_pasos);
        btn_start = (Button) findViewById(R.id.btn_start);
        btn_stop = (Button) findViewById(R.id.btn_stop);


        //en cas de clicar start iniciem el contador i sensor
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_passos.setVisibility(View.VISIBLE);
                numSteps = 0;
                sensorManager.registerListener(MainActivity.this, accel, SensorManager.SENSOR_DELAY_FASTEST);
            }
        });

        //en cas de clicar stop parem el contador i sensor
        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sensorManager.unregisterListener(MainActivity.this);
                tv_passos.setVisibility(View.INVISIBLE);
            }
        });

    }

    //Mètode que en cas de que el sensor canvi, envia la informació
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            Detectorpasses.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}