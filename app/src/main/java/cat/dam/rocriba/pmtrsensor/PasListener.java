package cat.dam.rocriba.pmtrsensor;

//Interfície que conta els pasos
public interface PasListener {
    public void step(long timeNs);
}
